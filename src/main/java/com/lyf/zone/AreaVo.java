package com.lyf.zone;

public class AreaVo {

	private String name ;
	private String code ;
	
	private String parentCode ;
	
	// 1 省 2-地市 3-区县 4-街道 5-村
	private int level ;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	public int getLevel() {
		return level;
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + "," + code + "," + level + "," + (parentCode == null || parentCode.equals("null") ? "" : parentCode);
	}
	
	public String toSql() {
		if(parentCode == null || parentCode.equals("null")) {
			return "insert into sys_zone(name,code,level) values('" + name + "','" + code +"'," + level + ");";
		} else {
			return "insert into sys_zone(name,code,level,parent_code) values('" + name + "','" + code +"'," + level + ",'" + parentCode + "');";
		}
		
	}
}
