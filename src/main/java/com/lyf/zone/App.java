package com.lyf.zone;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;

import us.codecraft.webmagic.Spider;

/**
 *  爬虫执行入口
 * @author lyf
 *
 */
public class App {
	
	/**
	 * 爬取数据的年份
	 */
	static String year = "2021" ;
	
	// sql输出路径
	private static String sqlPath = "./2021/data.sql" ;
	
	// csv输出路径
	private static String csvPath = "./2021/data.csv" ;
	
	// 爬取深度  1-省 2-地市  3-区县 4-乡镇/街道 5-村/社区
	private static int maxLevel = 4;
	
	// 起始区划 从某省开始 就XX譬如陕西:61 , 地市xxxx,譬如西安 6101,  爬取全国全量 则为空即可
	private static String start = "" ;
	
	// 线程数量
	private static int threadNum = 10 ;
	
	
	// 是否每一层的数据都单独存储 false的话则每一级一个文件 按照文件名_level.csv 结构
	private static boolean outFull = true ;
	
	public static void main(String[] args) {
		
		
		
		if(StringUtils.isNotBlank(sqlPath) && outFull) {
			// 初始化建表语句
			String createSql = " create table sys_zone(code varchar(12) primary key , name varchar(50) , level int , parent_code varchar(12)); "  ;
			WriteUtils.write(Arrays.asList(createSql), sqlPath , false);
		}
		
		if(StringUtils.isNotBlank(csvPath) && outFull) {
			// 写一个csv头
			String header = "name,code,level,parentCode"  ;
			WriteUtils.write(Arrays.asList(header), csvPath , false);
		}
		
		String url = "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/" + 2021;
		
		// 根据start 拼第一次的url
		if(StringUtils.isNotBlank(start)) {
			//"http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2020/61.html"
			
			// 省市区镇村的结构为 22233
			int len = start.length() ;
			
			
			// 判断是否长度符合规范 2 4 6 9 
			if(len != 2 && len != 4 && len != 6 && len != 9) {
				System.err.println("起始区划不符合长度规范,长度应该为省:2位,地市:4位,区县:6位,街道/乡镇:9位");
				System.exit(-1);
			}
			
			if(len > 2) {
				url += "/" + start.substring(0,2);
			}
			if(len > 4) {
				url += "/" + start.substring(2,4);
			}
			if(len > 6) {
				url += "/" + start.substring(4,6);
			}
			if(len > 9) {
				url += "/" + start.substring(6,9);
			}
			 
			url += "/" + start + ".html";
			
		} else {
			url += "/index.html"; 
		}
		
		AreaPage apage = new AreaPage();
		apage.setCsvPath(csvPath);
		apage.setSqlPath(sqlPath);
		apage.setMaxLevel(maxLevel);
		apage.setOutFull(outFull);
		
		Spider.create(apage).addUrl(url).thread(threadNum).run();
	}

}
