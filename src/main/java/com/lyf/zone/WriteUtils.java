package com.lyf.zone;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.IOUtils;

public class WriteUtils {
 
	public final static Byte  locks1 = 0;
	public final static Byte  locks2 = 0;
	
	public static   void write(List<?> list , String path  ) {
		synchronized(locks1) {
			write( list ,   path ,true );
		}
	
	}
	
	public static  void write(List<?> list , String path , boolean append) {
		synchronized(locks2) {
			File file = new File(path);
	    	
	    	if(!file.getParentFile().exists()) {
	    		file.getParentFile().mkdirs();
	    	}
	    	
	    	FileOutputStream out;
			try {
				out = new FileOutputStream(file, append);
				IOUtils.writeLines(list, null, out);
				
				out.flush();
				out.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    	
	}
}
