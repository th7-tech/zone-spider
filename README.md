# zone-spider

#### 介绍
根据国家统计局网站爬取最新行政区划数据，包含省、市、区县、乡镇/街道、村/社区 5级数据。

#### 数据来源
http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/ 国家统计局官方网站


#### 使用说明

将项目直接导入eclipse，idea等工具后运行App.java 即可。

目录2020中为2020年国家统计局中1-4级的数据（省-市-区县-街道/乡镇）。
目录2021中为2021年国家统计局中1-4级的数据（省-市-区县-街道/乡镇）。
 
#### 功能

1.  可以从任意一级开始爬取,可以指定最多爬取层级，譬如只爬取西安市下所有的区县和街道/乡镇,不需要获取到村级。
2.  多线程调度，线程数量可以自定义。
3.  输出格式包含csv和sql语句，方便使用。

### 输出格式

**1、CSV**

| 区划代码 | 区划名称 | 区划层级 | 上级区划代码 | 
|-----------|------------|--------|----------|
|610100000000| 西安市 | 2 | 610000000000 |


**2、SQL** 
```
 
create table sys_zone (

	code	varchar(12) primary key , 	-- 区划代码
	name	varchar(100) ,				--区划名称
	level	int , 						-- 区划层级1-省 2-地市 3-区县 4-乡镇/街道 5-社区/村
	parent_code	varchar(12)				-- 上级区划代码
 
);

```
